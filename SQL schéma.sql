SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Activite`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Activite` (
  `idAcvtivite` VARCHAR(10) NOT NULL ,
  `nomActivite` VARCHAR(50) NOT NULL ,
  `descrActivite` VARCHAR(250) NULL ,
  `dataActivite` DATETIME NOT NULL ,
  `idResponsActivite` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idAcvtivite`) ,
  UNIQUE INDEX `idResponsActivite_UNIQUE` (`idResponsActivite` ASC) ,
  UNIQUE INDEX `idAcvtivite_UNIQUE` (`idAcvtivite` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Volontaire`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Volontaire` (
  `idVolontaire` VARCHAR(10) NOT NULL ,
  `nomVolontaire` VARCHAR(45) NOT NULL ,
  `prenomVolontaire` VARCHAR(45) NOT NULL ,
  `login` VARCHAR(45) NOT NULL ,
  `mot_de_passe` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idVolontaire`) ,
  UNIQUE INDEX `idVolontaire_UNIQUE` (`idVolontaire` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Activite_has_Volontaire`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Activite_has_Volontaire` (
  `Activite_idAcvtivite` VARCHAR(10) NOT NULL ,
  `Volontaire_idVolontaire` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`Activite_idAcvtivite`, `Volontaire_idVolontaire`) ,
  INDEX `fk_Activite_has_Volontaire_Activite` (`Activite_idAcvtivite` ASC) ,
  INDEX `fk_Activite_has_Volontaire_Volontaire1` (`Volontaire_idVolontaire` ASC) ,
  CONSTRAINT `fk_Activite_has_Volontaire_Activite`
    FOREIGN KEY (`Activite_idAcvtivite` )
    REFERENCES `mydb`.`Activite` (`idAcvtivite` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Activite_has_Volontaire_Volontaire1`
    FOREIGN KEY (`Volontaire_idVolontaire` )
    REFERENCES `mydb`.`Volontaire` (`idVolontaire` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
